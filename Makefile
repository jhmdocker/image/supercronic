UBUNTU_VERSION=24.04
VERSION=v0.2.32
IMG=registry.gitlab.com/jhmdocker/image/supercronic

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		--build-arg SUPERCRONIC_VERSION=$(VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(VERSION)
	docker push $(IMG)
	docker push $(IMG):$(VERSION)
