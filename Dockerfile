ARG UBUNTU_VERSION=ND
FROM registry.gitlab.com/jhmdocker/image/ubuntu:${UBUNTU_VERSION}

## Sintaxe do crontab
## https://github.com/aptible/supercronic/tree/master/cronexpr
##

ARG SUPERCRONIC_VERSION=ND

# Latest releases available at https://github.com/aptible/supercronic/releases
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/${SUPERCRONIC_VERSION}/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=7da26ce6ab48d75e97f7204554afe7c80779d4e0 \
    SUPERCRONIC_CMD_OPTIONS="-split-logs" \
    SUPERCRONIC_CRONFILE=/etc/supercronic.cron

RUN echo SUPERCRONIC_VERSION=$SUPERCRONIC_VERSION \
    && echo SUPERCRONIC_URL=$SUPERCRONIC_URL \
    && curl -fsSLO "$SUPERCRONIC_URL" \
    && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
    && chmod +x "$SUPERCRONIC" \
    && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
    && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

COPY rootfs /

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
